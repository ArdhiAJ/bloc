import 'package:bloc_api_exercises/model/create_response_movie_detail.dart';
import 'package:equatable/equatable.dart';

class CartMovie extends Equatable {
  final MovieDetails movieDetails;
  final int quantity;

  CartMovie(this.movieDetails, {this.quantity = 0});

  @override
  List<Object> get props => [movieDetails];
}