import 'package:equatable/equatable.dart';

class Movie extends Equatable {
  final String title;
  final String overview;
  final String posterPath;
  final int id;
  final String releaseDate;

  Movie({this.title, this.overview, this.posterPath, this.id, this.releaseDate});

  factory Movie.fromJson(Map<String, dynamic> json) =>
      Movie(title: json['title'], overview: json['overview'], posterPath: json['poster_path'], id: json['id'], releaseDate: json['release_date']);

  @override
  List<Object> get props => [title, overview, posterPath, id, releaseDate];
}
