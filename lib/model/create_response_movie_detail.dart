import 'package:equatable/equatable.dart';

class MovieDetails extends Equatable {
  final String backdropPath;
  final List genres;
  final String tagLine;
  final String posterPath;
  final String overview;
  final double voteAverage;

  MovieDetails({this.backdropPath, this.genres, this.tagLine, this.posterPath, this.overview, this.voteAverage});

  factory MovieDetails.fromJson(Map<String, dynamic> json) =>
      MovieDetails(
        backdropPath: json['backdropPath'], 
        genres: json['genres'], 
        tagLine: json['tagLine'],
        posterPath: json['posterPath'], 
        overview: json['overview'], 
        voteAverage: json['voteAverage']
      );

  @override
  List<Object> get props => [];
}
