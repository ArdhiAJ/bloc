part of 'movie_detail_bloc.dart';

@immutable
abstract class MovieDetailEvent {}

class FetchMovieDetail extends MovieDetailEvent {
  final int movieID;

  FetchMovieDetail(this.movieID);
}
