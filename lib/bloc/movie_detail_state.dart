part of 'movie_detail_bloc.dart';

@immutable
abstract class MovieDetailState {}

class MovieDetailInitial extends MovieDetailState {}

class MovieDetailLoaded extends MovieDetailState {
  final List<MovieDetails> details;

  MovieDetailLoaded(this.details);
}
