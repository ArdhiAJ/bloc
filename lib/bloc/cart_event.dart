part of 'cart_bloc.dart';

abstract class CartEvent extends Equatable {
  const CartEvent();
}

class AddToCart extends CartEvent{
  final MovieDetails movieDetails;

  AddToCart(this.movieDetails);

  @override
  List<Object> get props => [movieDetails];
}