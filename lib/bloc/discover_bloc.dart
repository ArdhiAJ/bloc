import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../model/create_response.dart';
import '../service/movie_services.dart';

part 'discover_event.dart';
part 'discover_state.dart';

class DiscoverBloc extends Bloc<DiscoverEvent, DiscoverState> {
  @override
  DiscoverState get initialState => DiscoverInitial();

  @override
  Stream<DiscoverState> mapEventToState(
    DiscoverEvent event,
  ) async* {
    if(event is FetchMovie){
      if(state is DiscoverInitial){
        List<Movie> movies = await MovieServices.getMovies(1);
        yield DiscoverLoaded(movies);
      }
    }
  }
}
