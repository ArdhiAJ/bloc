part of 'discover_bloc.dart';

abstract class DiscoverEvent extends Equatable {
  const DiscoverEvent();
}

class FetchMovie extends DiscoverEvent{
  @override
  List<Object> get props => [];
}

