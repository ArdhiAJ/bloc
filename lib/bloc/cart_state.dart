part of 'cart_bloc.dart';
//hapus abstrat 
class CartState extends Equatable {
  final List<CartMovie> cartMovie;
  CartState(this.cartMovie);

  @override
  List<Object> get props => [cartMovie];
}

class CartInitial extends CartState {
  CartInitial(List<CartMovie> cartMovie) : super(cartMovie);

  @override
  List<Object> get props => [];
}
