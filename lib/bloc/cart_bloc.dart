import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bloc_api_exercises/model/cart_movie.dart';
import 'package:bloc_api_exercises/model/create_response_movie_detail.dart';
import 'package:equatable/equatable.dart';

part 'cart_event.dart';
part 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  @override
  CartState get initialState => CartInitial([]);

  @override
  Stream<CartState> mapEventToState(
    CartEvent event,
  ) async* {
    if (event is AddToCart) {
      int selectedIndex = state.cartMovie.indexOf(CartMovie(event.movieDetails));
      CartMovie movie;
      if (selectedIndex != -1) {
        movie = state.cartMovie.removeAt(selectedIndex);
      } else {
        movie = CartMovie(event.movieDetails);
        
      }
      print("index: " + selectedIndex.toString());
      print(movie.quantity);
      yield CartState(state.cartMovie + [CartMovie(movie.movieDetails, quantity: movie.quantity + 1)]);
    }
  }
}
