part of 'discover_bloc.dart';

abstract class DiscoverState extends Equatable {
  const DiscoverState();
}

class DiscoverInitial extends DiscoverState {
  @override
  List<Object> get props => [];
}

class DiscoverLoaded extends DiscoverState{
  final List<Movie> movies;

  DiscoverLoaded(this.movies);

  @override
  List<Object> get props => [movies];

}
