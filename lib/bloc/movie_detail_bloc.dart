import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bloc_api_exercises/model/create_response_movie_detail.dart';
import 'package:bloc_api_exercises/service/movie_services.dart';
import 'package:meta/meta.dart';

part 'movie_detail_event.dart';
part 'movie_detail_state.dart';

class MovieDetailBloc extends Bloc<MovieDetailEvent, MovieDetailState> {
  @override
  MovieDetailState get initialState => MovieDetailInitial();

  @override
  Stream<MovieDetailState> mapEventToState(
    MovieDetailEvent event,
  ) async* {
    if (event is FetchMovieDetail) {
      List<MovieDetails> details =
          await MovieServices.getMoviesDetail(event.movieID);
      print(details);
      yield MovieDetailLoaded(details);
    }
  }
}
