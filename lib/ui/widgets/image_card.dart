import 'package:flutter/material.dart';
import 'package:bloc_api_exercises/model/create_response.dart';
import 'package:google_fonts/google_fonts.dart';

class ImageCard extends StatelessWidget {
  final Movie movie;

  ImageCard(this.movie);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              alignment: Alignment.center,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(15.0),
                child: Image(
                  image: NetworkImage("https://image.tmdb.org/t/p/original" +
                      movie.posterPath),
                  width: MediaQuery.of(context).size.width * 0.3,
                  // height: MediaQuery.of(context).size.width * 0.7,
                  fit: BoxFit.fitWidth,
                ),
              )),
        ],
      ),
    );
  }
}
