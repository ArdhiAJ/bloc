import 'package:flutter/material.dart';
import 'package:bloc_api_exercises/model/create_response.dart';
import 'package:google_fonts/google_fonts.dart';

class MovieCard extends StatelessWidget {
  final Movie movie;

  MovieCard(this.movie);

  @override
  Widget build(BuildContext context) {
    // print("${movie.id}");
    // return Container(
    //   margin: EdgeInsets.all(5),
    //   padding: EdgeInsets.all(10),
    //   decoration: BoxDecoration(
    //       color: Colors.lightBlue[100],
    //       borderRadius: BorderRadius.circular(10),
    //       boxShadow: [
    //         BoxShadow(color: Colors.black38, blurRadius: 3, spreadRadius: 3)
    //       ]),
    //   child: Row(
    //     children: <Widget>[
    //       Column(
    //         crossAxisAlignment: CrossAxisAlignment.start,
    //         children: <Widget>[
    //           Container(
    //               alignment: Alignment.center,
    //               child: Image(
    //                 image: NetworkImage(
    //                     "https://image.tmdb.org/t/p/w200" + movie.posterPath),
    //                 width: 200,
    //                 height: 200,
    //               )),
    //           Padding(
    //             padding: const EdgeInsets.only(top: 10, bottom: 10),
    //             child: Text(
    //               "Title: ${movie.title}",
    //               style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
    //             ),
    //           ),
    //           Container(
    //             width: MediaQuery.of(context).size.width * 0.9,
    //             child: Text("Overview: ${movie.overview}", textAlign: TextAlign.justify,)),
    //         ],
    //       )
    //     ],
    //   ),
    // );
    return new Card(
      margin: EdgeInsets.fromLTRB(15, 15, 15, 0),
      color: Colors.grey[50],
      // elevation: 5,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                alignment: Alignment.center,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(15.0),
                  child: Image(
                    image: NetworkImage("https://image.tmdb.org/t/p/original" +
                        movie.posterPath),
                    width: MediaQuery.of(context).size.width * 0.3,
                    // height: MediaQuery.of(context).size.width * 0.7,
                    fit: BoxFit.fitWidth,
                  ),
                )),
            Padding(
              padding: const EdgeInsets.only(top: 5, left: 15.0),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.2,
                child: Text(
                  "${movie.title}",
                  style:
                      GoogleFonts.oswald(fontSize: 14, color: Colors.redAccent, fontWeight: FontWeight.bold), textAlign: TextAlign.start,
                ),
              ),
            ),
            // Padding(
            //   padding: const EdgeInsets.only(left: 10),
            //   child: new Column(
            //     children: <Widget>[
            //       Padding(
            //         padding: const EdgeInsets.only(bottom: 10.0),
            //         child: Container(
            //           width: MediaQuery.of(context).size.width * 0.5,
            //           child: Text(
            //             "Title: ${movie.title}",
            //             style:
            //                 GoogleFonts.notoSans(fontSize: 14, color: Colors.white),
            //           ),
            //         ),
            //       ),

            //       // Container(
            //       //   width: MediaQuery.of(context).size.width * 0.5,
            //       //   child: Text(
            //       //     "Overview: ${movie.overview}",
            //       //     style: GoogleFonts.notoSans(color: Colors.white),
            //       //   ),
            //       // ),
            //     ],
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
