import 'package:bloc_api_exercises/service/movie_services.dart';
import 'package:bloc_api_exercises/ui/pages/detail_movie.dart';
import 'package:bloc_api_exercises/ui/pages/second_page.dart';
import 'package:bloc_api_exercises/ui/pages/view_cart.dart';
import 'package:bloc_api_exercises/ui/widgets/movie_card.dart';
import 'package:flutter/material.dart';
import 'package:bloc_api_exercises/model/create_response.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:bloc_api_exercises/ui/pages/home_page.dart';

import '../../bloc/discover_bloc.dart';
import '../../bloc/discover_bloc.dart';
import '../../bloc/discover_bloc.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  Movie response;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[300],
        appBar: new PreferredSize(
          preferredSize: Size(double.infinity, 50),
          child: new ClipRRect(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(25),
                bottomRight: Radius.circular(25)),
            child: new AppBar(
              backgroundColor: Colors.white,
              leading: Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: new Icon(
                    Icons.local_movies,
                    color: Colors.redAccent,
                  )),
              title: Center(
                child: new Text(
                  "KAA Movies App",
                  style: GoogleFonts.merriweather(
                      color: Colors.redAccent,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
              ),
              actions: <Widget>[
                Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: GestureDetector(
                      onTap: (){
                        Navigator.push(context, new MaterialPageRoute(builder: (context) => new ViewCart(response)));
                      },
                      child: new Icon(
                        Icons.shopping_cart,
                        color: Colors.redAccent,
                      ),
                    )),
              ],
            ),
          ),
        ),
        body: BlocBuilder<DiscoverBloc, DiscoverState>(
          builder: (_, discoverState) {
            if (discoverState is DiscoverLoaded) {
              return ListView(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 15, left: 20),
                        child: Container(
                          alignment: Alignment.topLeft,
                          child: new Text(
                            "Popular TV Shows",
                            style: GoogleFonts.merriweather(
                                fontSize: 18,
                                color: Colors.grey,
                                fontWeight: FontWeight.bold),
                            textAlign: TextAlign.start,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15, right: 20),
                        child: Container(
                          alignment: Alignment.topLeft,
                          child: new Text(
                            "More",
                            style: GoogleFonts.merriweather(
                                fontSize: 18,
                                color: Colors.redAccent,
                                fontWeight: FontWeight.bold),
                            textAlign: TextAlign.start,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.all(15),
                    // width: 500,
                    height: 260,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: discoverState.movies.length,
                      itemBuilder: (context, int index) => Container(
                        margin: EdgeInsets.only(bottom: 15),
                        child: GestureDetector(
                            onTap: () async {
                              response = discoverState.movies[index];
                              print(response);
                              await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => DetailMovie(
                                          discoverState.movies[index].id, response)));
                            },
                            child: MovieCard(discoverState.movies[index])),
                        // child: Column(
                        //   crossAxisAlignment: CrossAxisAlignment.start,
                        //   children: <Widget>[
                        //     Text("Title: ${movies[index].title}"),
                        //     Text("Overview: ${movies[index].overview}"),
                        //   ],
                        // ),
                      ),
                    ),
                    // child: ListView.builder(
                    //   scrollDirection: Axis.horizontal,
                    //   itemCount: discoverState.movies.length,
                    //   itemBuilder: (context, int index) => Container(
                    //     margin: EdgeInsets.only(bottom: 15),
                    //     child: GestureDetector(
                    //       onTap: (){
                    //         Navigator.push(context, new MaterialPageRoute(builder: (context) => new SeconPage(discoverState.movies[index])));
                    //       },
                    //         child: MovieCard(discoverState.movies[index])),
                    //     // child: HomePage(discoverState.movies[index]),
                    //     // child: Column(
                    //     //   crossAxisAlignment: CrossAxisAlignment.start,
                    //     //   children: <Widget>[
                    //     //     Text("Title: ${movies[index].title}"),
                    //     //     Text("Overview: ${movies[index].overview}"),
                    //     //   ],
                    //     // ),
                    //   ),
                    // ),
                  ),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //   children: <Widget>[
                  //     Padding(
                  //       padding: const EdgeInsets.only(top: 15, left: 20),
                  //       child: Container(
                  //         alignment: Alignment.topLeft,
                  //         child: new Text(
                  //           "Actions",
                  //           style: GoogleFonts.domine(
                  //               fontSize: 18,
                  //               color: Colors.grey,
                  //               fontWeight: FontWeight.bold),
                  //           textAlign: TextAlign.start,
                  //         ),
                  //       ),
                  //     ),
                  //     Padding(
                  //       padding: const EdgeInsets.only(top: 15, right: 20),
                  //       child: Container(
                  //         alignment: Alignment.topLeft,
                  //         child: new Text(
                  //           "More",
                  //           style: GoogleFonts.domine(
                  //               fontSize: 18,
                  //               color: Colors.redAccent,
                  //               fontWeight: FontWeight.bold),
                  //           textAlign: TextAlign.start,
                  //         ),
                  //       ),
                  //     ),
                  //   ],
                  // ),
                  // Container(
                  //   margin: EdgeInsets.all(15),
                  //   width: 500,
                  //   height: 260,
                  //   decoration: BoxDecoration(
                  //       color: Colors.white,
                  //       borderRadius: BorderRadius.circular(10)),
                  //   child: ListView.builder(
                  //     scrollDirection: Axis.horizontal,
                  //     itemCount: discoverState.movies.length,
                  //     itemBuilder: (context, int index) => Container(
                  //       margin: EdgeInsets.only(bottom: 15),
                  //       child: MovieCard(discoverState.movies[index]),
                  //       // child: HomePage(discoverState.movies[index]),
                  //       // child: Column(
                  //       //   crossAxisAlignment: CrossAxisAlignment.start,
                  //       //   children: <Widget>[
                  //       //     Text("Title: ${movies[index].title}"),
                  //       //     Text("Overview: ${movies[index].overview}"),
                  //       //   ],
                  //       // ),
                  //     ),
                  //   ),
                  // ),
                ],
              );
            } else {
              return Center(
                child: SizedBox(
                  width: 25,
                  height: 25,
                  child: CircularProgressIndicator(),
                ),
              );
            }
          },
        ));
  }
}
