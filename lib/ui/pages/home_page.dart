import 'package:flutter/material.dart';
import 'package:bloc_api_exercises/model/create_response.dart';
import 'package:google_fonts/google_fonts.dart';

class HomePage extends StatelessWidget {
  final Movie movie;

  HomePage(this.movie);

  @override
  Widget build(BuildContext context) {
    return new Container(
      width: 350,
      child: new ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Card(
            margin: EdgeInsets.all(15),
            color: Color(0xff007022),
            elevation: 5,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                      alignment: Alignment.center,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(15.0),
                        child: Image(
                          image: NetworkImage(
                              "https://image.tmdb.org/t/p/original" +
                                  movie.posterPath),
                          width: MediaQuery.of(context).size.width * 1,
                          // height: MediaQuery.of(context).size.width * 0.7,
                          fit: BoxFit.fitWidth,
                        ),
                      )),
                  Padding(
                    padding: const EdgeInsets.only(top: 10, bottom: 10),
                    child: Text(
                      "Title: ${movie.title}",
                      style: GoogleFonts.notoSans(
                          fontSize: 20, color: Colors.white),
                    ),
                  ),
                  Text(
                    "Overview: ${movie.overview}",
                    style: GoogleFonts.notoSans(color: Colors.white),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
