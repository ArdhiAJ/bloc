import 'package:bloc_api_exercises/bloc/cart_bloc.dart';
import 'package:bloc_api_exercises/bloc/discover_bloc.dart';
import 'package:bloc_api_exercises/bloc/movie_detail_bloc.dart';
import 'package:bloc_api_exercises/model/create_response.dart';
import 'package:bloc_api_exercises/model/create_response_movie_detail.dart';
import 'package:bloc_api_exercises/ui/widgets/image_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

class DetailMovie extends StatefulWidget {
  final int movieID;
  final MovieDetails selectedCartMovie;
  final Movie movie;

  DetailMovie(this.movieID, this.movie, {this.selectedCartMovie});

  @override
  _DetailMovieState createState() => _DetailMovieState();
}

class _DetailMovieState extends State<DetailMovie> {
  int movieID;
  MovieDetails selectedCartMovie;

  @override
  void initState() {
    movieID = widget.movieID;
    selectedCartMovie = widget.selectedCartMovie;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: ListView(
        children: <Widget>[
          new Image(
            image: NetworkImage("https://image.tmdb.org/t/p/original" +
                widget.movie.posterPath),
            width: MediaQuery.of(context).size.width,
            height: 180,
            fit: BoxFit.cover,
          ),
          new Container(
            child: BlocProvider(
              create: (_) => MovieDetailBloc()..add(FetchMovieDetail(movieID)),
              child: BlocBuilder<MovieDetailBloc, MovieDetailState>(
                builder: (_, discoverState) {
                  if (discoverState is MovieDetailLoaded) {
                    return Column(
                      children: <Widget>[
                        Container(
                          color: Colors.white,
                          height: 320,
                          child: ListView.builder(
                              // physics: NeverScrollableScrollPhysics(),
                              itemCount: discoverState.details.length,
                              itemBuilder: (_, int index) {
                                selectedCartMovie =
                                    discoverState.details[index];
                                print(selectedCartMovie);
                                return Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Row(
                                        children: <Widget>[
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            child: new Image(
                                              image: NetworkImage(
                                                ("https://image.tmdb.org/t/p/original" +
                                                    "${discoverState.details[index].posterPath}"),
                                              ),
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.3,
                                              // height: MediaQuery.of(context).size.width * 0.7,
                                              fit: BoxFit.fitWidth,
                                            ),
                                          ),
                                          new SizedBox(
                                            width: 15,
                                          ),
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.6,
                                            child: new Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  "${widget.movie.title}",
                                                  style:
                                                      GoogleFonts.merriweather(
                                                          fontSize: 18,
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 15),
                                                  child: Text(
                                                    "${widget.movie.releaseDate}",
                                                    style: GoogleFonts.oswald(
                                                      fontSize: 15,
                                                      color: Colors.grey,
                                                    ),
                                                  ),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          bottom: 5),
                                                  child: Text(
                                                    "${discoverState.details[index].tagLine}",
                                                    style: GoogleFonts.oswald(
                                                      fontSize: 15,
                                                      color: Colors.grey,
                                                    ),
                                                  ),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          bottom: 5),
                                                  child: Text(
                                                    "${discoverState.details[index].voteAverage}",
                                                    style: GoogleFonts.oswald(
                                                      fontSize: 15,
                                                      color: Colors.black,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Text(
                                      "${discoverState.details[index].overview}",
                                      style: GoogleFonts.merriweather(
                                        fontSize: 16,
                                        color: Colors.black,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 10,
                                          left: 130, right: 130, bottom: 10),
                                      child: new RaisedButton(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        color: Colors.redAccent,
                                        onPressed: () {
                                          context.bloc<CartBloc>().add(
                                              AddToCart(selectedCartMovie));
                                        },
                                        child: new Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: <Widget>[
                                            new Icon(
                                              Icons.add_shopping_cart,
                                              color: Colors.white,
                                            ),
                                            new Text(
                                              "Buy",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                );
                              }),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 10, left: 20, bottom: 10),
                          child: Container(
                            alignment: Alignment.topLeft,
                            child: new Text(
                              "Similar Movies",
                              style: GoogleFonts.merriweather(
                                  fontSize: 18,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.start,
                            ),
                          ),
                        ),
                        new Container(
                          color: Colors.white,
                          child: BlocBuilder<DiscoverBloc, DiscoverState>(
                            builder: (_, discoverState) {
                              if (discoverState is DiscoverLoaded) {
                                return Container(
                                  margin: EdgeInsets.all(0),
                                  // width: 500,
                                  height: 180,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(10)),
                                  child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: discoverState.movies.length,
                                    itemBuilder: (context, int index) =>
                                        Container(
                                      child: ImageCard(
                                          discoverState.movies[index]),
                                    ),
                                  ),
                                );
                              }
                            },
                          ),
                        )
                      ],
                    );
                  } else {
                    return Center(
                      child: SizedBox(
                        width: 25,
                        height: 25,
                        child: CircularProgressIndicator(),
                      ),
                    );
                  }
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
