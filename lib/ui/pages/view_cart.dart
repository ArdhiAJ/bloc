import 'package:bloc_api_exercises/bloc/cart_bloc.dart';
import 'package:bloc_api_exercises/model/create_response.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

class ViewCart extends StatefulWidget {
  final Movie movie;

  ViewCart(this.movie);

  @override
  _ViewCartState createState() => _ViewCartState();
}

class _ViewCartState extends State<ViewCart> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      appBar: new PreferredSize(
        preferredSize: Size(double.infinity, 50),
        child: new ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(25),
              bottomRight: Radius.circular(25)),
          child: new AppBar(
            backgroundColor: Colors.white,
            leading: Padding(
                padding: const EdgeInsets.only(right: 10),
                child: new Icon(
                  Icons.local_movies,
                  color: Colors.redAccent,
                )),
            title: Center(
              child: new Text(
                "Selected Cart",
                style: GoogleFonts.merriweather(
                    color: Colors.redAccent,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
      ),
      body: BlocBuilder<CartBloc, CartState>(builder: (_, cartState) {
        return ListView(
          children: <Widget>[
            Container(
              height: 600,
              color: Colors.grey[300],
              child: ListView.builder(
                itemCount: cartState.cartMovie.length,
                itemBuilder: (context, index) {
                  print(cartState.cartMovie[index].movieDetails.voteAverage
                      .toString());
                  return new Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      margin: EdgeInsets.all(20),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: new Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(right: 15),
                              child: Container(
                                  alignment: Alignment.center,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(10.0),
                                    child: Image(
                                      image: NetworkImage(
                                          "https://image.tmdb.org/t/p/original" +
                                              widget.movie.posterPath),
                                      width: MediaQuery.of(context).size.width *
                                          0.2,
                                      // height: MediaQuery.of(context).size.width * 0.7,
                                      fit: BoxFit.fitWidth,
                                    ),
                                  )),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.6,
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 10),
                                    child: Text(
                                      widget.movie.title,
                                      style: GoogleFonts.merriweather(
                                          fontSize: 16,
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 5),
                                    child: Text(
                                      cartState.cartMovie[index].movieDetails
                                          .voteAverage
                                          .toString(),
                                      style: GoogleFonts.oswald(
                                          fontSize: 14,
                                          color: Colors.grey,
                                        ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 5),
                                    child: Text(
                                      cartState
                                          .cartMovie[index].movieDetails.tagLine
                                          .toString(),
                                      style: GoogleFonts.oswald(
                                          fontSize: 14,
                                          color: Colors.grey,
                                          ),
                                    ),
                                  ),

                                  // Text(cartState
                                  //     .cartMovie[index].movieDetails.genres
                                  //     .toString()),
                                ],
                              ),
                            )
                          ],
                        ),
                      ));
                },
              ),
            ),
          ],
        );
      }),
    );
  }
}
