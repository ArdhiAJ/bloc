import 'package:bloc_api_exercises/bloc/discover_bloc.dart';
import 'package:bloc_api_exercises/model/create_response.dart';
import 'package:bloc_api_exercises/ui/widgets/movie_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

class SeconPage extends StatefulWidget {
  final Movie movie;

  SeconPage(this.movie);

  @override
  _SeconPageState createState() => _SeconPageState();
}

class _SeconPageState extends State<SeconPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: new ListView(
        children: <Widget>[
          // new SizedBox(
          //   height: 25,
          // ),
          new Image(
            image: NetworkImage(
                "https://image.tmdb.org/t/p/original" + widget.movie.posterPath),
            width: MediaQuery.of(context).size.width,
            height: 180,
            fit: BoxFit.cover,
          ),
          new Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: new Column(
                children: <Widget>[
                  new Row(
                    children: <Widget>[
                      ClipRRect(
                         borderRadius: BorderRadius.circular(15.0),
                        child: Image(
                          image: NetworkImage(
                              "https://image.tmdb.org/t/p/original" +
                                  widget.movie.posterPath),
                          width: MediaQuery.of(context).size.width * 0.3,
                          // height: MediaQuery.of(context).size.width * 0.7,
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                      new SizedBox(
                        width: 15,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.5,
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "${widget.movie.title}",
                              style: GoogleFonts.domine(
                                  fontSize: 20,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              "${widget.movie.releaseDate}",
                              style: GoogleFonts.domine(
                                  fontSize: 15,
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Text(
                    "${widget.movie.overview}",
                    style: GoogleFonts.notoSans(
                        fontSize: 20,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  ),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      RaisedButton(
                        color: Colors.redAccent,
                        onPressed: () {},
                        child: Text(
                          "RENT",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                      RaisedButton(
                        color: Colors.redAccent,
                        onPressed: () {},
                        child: Text(
                          "BUY",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          new SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15, left: 20, bottom: 20),
            child: Container(
              alignment: Alignment.topLeft,
              child: new Text(
                "Similar Movies",
                style: GoogleFonts.domine(
                    fontSize: 18,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.start,
              ),
            ),
          ),
          new Container(
            color: Colors.white,
            child: BlocBuilder<DiscoverBloc, DiscoverState>(
              builder: (_, discoverState) {
                if (discoverState is DiscoverLoaded) {
                  return Container(
                    margin: EdgeInsets.all(15),
                    // width: 500,
                    height: 260,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: discoverState.movies.length,
                      itemBuilder: (context, int index) => Container(
                        margin: EdgeInsets.only(bottom: 15),
                        child: MovieCard(discoverState.movies[index]),
                        // child: HomePage(discoverState.movies[index]),
                        // child: Column(
                        //   crossAxisAlignment: CrossAxisAlignment.start,
                        //   children: <Widget>[
                        //     Text("Title: ${movies[index].title}"),
                        //     Text("Overview: ${movies[index].overview}"),
                        //   ],
                        // ),
                      ),
                    ),
                  );
                }
                // else {
                //   return Center(
                //     child: SizedBox(
                //       width: 25,
                //       height: 25,
                //       child: CircularProgressIndicator(),
                //     ),
                //   );
                // }
              },
            ),
          )
        ],
      ),
    );
  }
}
