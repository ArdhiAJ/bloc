import 'dart:convert';
import 'package:bloc_api_exercises/model/create_response_movie_detail.dart';
import 'package:http/http.dart' as http;

class MovieDetailServices {
  static Future<List<MovieDetails>> getMovies(int id, {http.Client httpClient}) async {
    if (httpClient == null) {
      httpClient = http.Client();
    }

    var response =
        await httpClient.get("https://api.themoviedb.org/3/movie/$id?api_key=f9553c730ba83e4634b518f6481b0457&language=en-US");

    if (response.statusCode == 200) {
      var result = json.decode(response.body);
      String movieData = result['budget'];
      String movieData1 = result['budget'];
      // String movieData = result['budget'];
      // return movieData.map((element) => MovieDetails.fromJson(element)).toList();
    }
    return null;
  }
}
