import 'dart:convert';
import 'package:bloc_api_exercises/model/create_response.dart';
import 'package:bloc_api_exercises/model/create_response_movie_detail.dart';
import 'package:bloc_api_exercises/shared/shared_value.dart';
import 'package:http/http.dart' as http;

class MovieServices {
  static Future<List<Movie>> getMovies(int page, {http.Client httpClient}) async {
    if (httpClient == null) {
      httpClient = http.Client();
    }

    var response =
        await httpClient.get("https://api.themoviedb.org/3/discover/movie?api_key=f9553c730ba83e4634b518f6481b0457&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=$page");

    if (response.statusCode == 200) {
      var result = json.decode(response.body);
      List movieData = result['results'];
      return movieData.map((element) => Movie.fromJson(element)).toList();
    }
    return null;
  }

  static Future<List<MovieDetails>> getMoviesDetail(int id, {http.Client httpClient}) async {
    if (httpClient == null) {
      httpClient = http.Client();
    }
    var response =
        await httpClient.get("$urlDetail/$id?api_key=$apiKey&language=en-US"); 
    if (response.statusCode == 200) {
      var result = json.decode(response.body);
      List data = [];
      var listData = {
          'backdropPath': result['backdrop_path'],
          'genres': result['genres'],
          'tagLine': result['tagline'],
          'posterPath': result['poster_path'],
          'overview': result['overview'],
          'voteAverage': result['vote_average'],
      };
      data.add(listData);
      print(data);
      return data.map((element) => MovieDetails.fromJson(element)).toList();
    } else {
      return null;
    }
  }
}
